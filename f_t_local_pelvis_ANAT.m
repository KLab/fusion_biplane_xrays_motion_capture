% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Define the local coordinate system of the pelvis based on anatomical points 
%              measured with medical imaging
%              Axis directions are consistent with the Conventional Gait Model
% Inputs     : RASI - Position of Right Anterior Superior Iliac Spine (n x 3)
%              LASI - Position of Left  Anterior Superior Iliac Spine (n x 3)
%              PSYM - Position of Pelvic Symphisis (n x 3)
% Outputs    : Rgp - Rotation Matrix global toward pelvis
%              Tgp - Transformation Matrix global toward pelvis
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [Rgp,Tgp] = f_t_local_pelvis_ANAT(RASI,LASI,PSYM)

n = size(RASI,1); % Frame number
mASI = (RASI + LASI) / 2;
% Axis definition
yp = f_t_Vnorm(LASI-RASI);
  z_temp = mASI-PSYM;
xp = f_t_Vnorm(f_t_cross(yp,z_temp));  
zp = f_t_Vnorm(f_t_cross(xp,yp)); 

% Rotation Matrix pelvis w.r.t ICS
Rgp = [permute(xp,[2 3 1])   permute(yp,[2 3 1])   permute(zp,[2 3 1])];
% Origin Correction, remove half a marker in x direction
Ogp = mASI;
% Homogeneous Matrix
ZEROS = zeros(1,1,n);
ONES  = ones(1,1,n);

Tgp=[Rgp permute(Ogp,[2 3 1]);ZEROS ZEROS ZEROS ONES];