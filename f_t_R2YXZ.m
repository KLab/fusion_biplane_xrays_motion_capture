% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Decomposes a Rotation  matrix into a YXZ sequence
% Inputs     : R - the 3x3xn rotation matrix (n is number of frames)
% Outputs    : a_out - joint angles
%              b_out - joint angles
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [a_out,b_out]=f_t_R2YXZ(R)

a(1,1,:)= atan2(R(1,3,:),R(3,3,:));
a(2,1,:)= asin(-R(2,3,:));
a(3,1,:)= atan2(R(2,1,:),R(2,2,:));
%
b(1,1,:)= atan2(-R(1,3,:),-R(3,3,:));
b(2,1,:)= rem(pi-asin(-R(2,3,:)) + pi , 2*pi )-pi; 
b(3,1,:)= atan2(-R(2,1,:),-R(2,2,:));
%
% Outputs
a_out = permute(a,[3 1 2]);
b_out = permute(b,[3 1 2]);

