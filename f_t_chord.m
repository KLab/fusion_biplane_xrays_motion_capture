% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Function of the CGM to compute the Knee Joint Centre or Ankle Joint Centre
% Inputs     : For the AJC, A1 = ANK, A2 = KJC, A3 = TIB, offset = ankle width
%              For the KJC, A1 = KNE, A2 = HJC, A3 = THI, offset = knee width
% Outputs    : Knee/Ankle Joint Center
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [P] = f_t_chord(offset,A1,A2,A3)

offset = offset/2; % knee width / 2
n = size(A1,1);
% offset: Knee Radius
I = A1; 
J = A2; 
K = A3; 
%
y = f_t_Vnorm(J-I);
x = f_t_Vnorm(f_t_cross(y,K-I));
z = f_t_Vnorm(f_t_cross(x,y));
% Rot Mat
R = [permute(x,[2 3 1])   permute(y,[2 3 1])   permute(z,[2 3 1])]; 
O = (J+I)/2; % Origin
%
d = f_t_norm(I-J);
theta = asin(offset./d)*2;
v_r = [zeros(1,1,n);permute(-d/2,[2 3 1]);zeros(1,1,n)];
%
ctheta = permute(cos(theta),[3 2 1]);
stheta = permute(sin(theta),[3 2 1]);
rot = [ones(1,1,n), zeros(1,2,n); ...
       zeros(1,1,n), ctheta,-stheta;...
       zeros(1,1,n), stheta, ctheta];
% 
P = permute(Mprod_array3(Mprod_array3(R,rot),v_r),[3 1 2]) + O;
