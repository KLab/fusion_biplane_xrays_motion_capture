Author     :   X. Gasparutto
               Kinesiology Laboratory (K-LAB)
               University of Geneva
               https://www.unige.ch/medecine/kinesiology
License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
               https://creativecommons.org/licenses/by-nc/4.0/legalcode
Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
               variability due to marker misplacements?" 
               X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
Date       :   December 2019
-------------------------------------------------------------------------
This work is licensed under the Creative Commons Attribution - 
NonCommercial 4.0 International License. To view a copy of this license, 
visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-------------------------------------------------------------------------

List of files:
-------------------------------------------------------------------------
- Patient_x_DoseInfo.csv : File storing the radiation dose information of the biplane xray measurement
- Patient_x_Frontal.dcm  : DICOM of the frontal image of the biplane xray measurement
- Patient_x_Sagittal.dcm : DICOM of the sagittal image of the biplane xray measurement
- Patient_x_mrkBPXR.csv  : File storing the anatomical points and skin markers identified on the 
                           biplane xray measurement with a toolbox available on the following link
			   (Data in mm)
			   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture
- Patient_x_Gait.c3d     : Motion capture file storing the trajectories of a subset of skin markers of
                           the Conventional Gait Model during one right gait cycle. Only the markers
		           relevant for this study were kept. (Data in mm)

Measurements details:
-------------------------------------------------------------------------
- Motion capture: 
	- Gait at self selected speed on a 10m walkway
	- System: 12 cameras Qualisys system (Oqus7+, QTM 2.14, Qualisys, G�teborg, Sweden)
	- Acquisition frequency: 100Hz
	- Software: QTM 2019
	- No filtering of the marker trajectories
	- No gap filling of the marker trajectories
	- Skin marker of the conventional gait model + intentionally misplaced markers:
		  Label/ Format / Dim. / Unit / Description 
		- RASI:   Real  / nx3  /  mm  / Right Anterior  Superior Iliac Spine
		- RASI2:  Real  / nx3  /  mm  / Right Anterior  Superior Iliac Spine misplaced
		- LASI:   Real  / nx3  /  mm  / Left  Anterior  Superior Iliac Spine
		- LASI2:  Real  / nx3  /  mm  / Left  Anterior  Superior Iliac Spine misplaced
		- RPSI:   Real  / nx3  /  mm  / Right Posterior Superior Iliac Spine
		- RPSI2:  Real  / nx3  /  mm  / Right Posterior Superior Iliac Spine misplaced
		- LPSI:   Real  / nx3  /  mm  / Left  Posterior Superior Iliac Spine
		- LPSI2:  Real  / nx3  /  mm  / Left  Posterior Superior Iliac Spine misplaced
		- RKNE:   Real  / nx3  /  mm  / Lateral Femoral Epicondyle of the Right Lower Limb
		- RTHI:   Real  / nx3  /  mm  / Wand Marker of the Right Thigh
		- RHEE:   Real  / nx3  /  mm  / Posterior aspect of the right heel 
		- RANK:   Real  / nx3  /  mm  / Lateral Malleolus of the Right Lower Limb
		
- Bi-Plane X-Ray:
	- System: EOS Imaging Inc., Paris, France 
	- Standard lower limb measurement of the EOS system, for clinical use
	- Standing posture, one foot in front of the other
	- Motion capture markers equipped: 
          	- Anatomical: RASI, RASI2, LASI, LASI2, Left & Right KNE and Left & Right KNI (medial femoral epicondyle)
	 	 - Cluster of marker: Thigh and Pelvis 
	
Pelvis Markers Misplacement:
-------------------------------------------------------------------------
- Patient a: Vertical offset
- Patient b: Axial Rotation 
- Patient c: Anteversion
- Patient d: Obliquity

Patient characteristics:
-------------------------------------------------------------------------
- Patient a: Female, 55 years old, 62kg, 163cm, right total hip arthroplasty (3 months post surgery)
- Patient b: Male, 72 years old, 71kg, 168cm, end sage right hip osteoarthritis (planned for arthroplasty)
- Patient c: Male, 71 years old, 68kg, 167cm, left total hip arthroplasty (3 months post surgery)
- Patient d: Female, 81 years old, 41kg, 164cm, end-stage left hip osteoarthritis (planned for arthroplasty)