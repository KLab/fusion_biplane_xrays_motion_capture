% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Compute local coordinate system of pelvis with the Conventional Gait Model convention
%              Marker size is in m
%              x-axis: frontal pointing forward
%              y-axis: lateral pointing left
%              z-axis: vertical pointing upward
% Inputs     : Row vectors indicating positions in the global coordinate system
% Outputs    : Rgp - Rotation Matrix global toward pelvis
%              Tgp - Transformation Matrix global toward pelvis
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [Rgp,Tgp] = f_t_local_pelvis_CGM(RASI,LASI,RPSI,LPSI,mrk_size)

n = size(RASI,1);
mPSI = (RPSI + LPSI) / 2;
mASI = (RASI + LASI) / 2;
% Axis definition
yp = f_t_Vnorm(LASI-RASI);
  x_temp = f_t_Vnorm(RASI-mPSI);
zp = f_t_Vnorm(f_t_cross(x_temp,yp)); 
xp = f_t_Vnorm(f_t_cross(yp,zp));
% Rotation Matrix pelvis w.r.t ICS
Rgp = [permute(xp,[2 3 1])   permute(yp,[2 3 1])   permute(zp,[2 3 1])];
% Origin Correction, remove half a marker in x direction
Ogp = mASI - xp * mrk_size /2;
% Homogeneous Matrix
ZEROS = zeros(1,1,n);
ONES  = ones(1,1,n);

Tgp=[Rgp permute(Ogp,[2 3 1]);ZEROS ZEROS ZEROS ONES];
