% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Compute thigh LCS & KJC in CGM convention
% Inputs     : KNE - Position of the knee lateral marker of the CGM (n x 3)
%              HJC - Position of the hip joint center (n x 3)
%              THI - Position of the wand marker of the thigh (n x 3)
% Outputs    : Rgp - Rotation Matrix global toward thigh
%              Tgp - Transformation Matrix global toward thigh
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [Rgt, KJC] = f_t_local_thigh_CGM(knee_width,KNE,HJC,THI)

n = size(KNE,1);

% 1 - Get Knee Joint Center with chord function
KJC = f_t_chord(knee_width,KNE,HJC,THI);

% 2 - Compute LCS
% 2.1 - Axis
zt = f_t_Vnorm(HJC - KJC);      % Down to Up
yt = f_t_Vnorm(KNE - KJC);      % Med to Lat
xt = f_t_Vnorm(f_t_cross(yt,zt)); % Post to Ant
% 2.2 - Rotation matrix
Rgt = [permute(xt,[2 3 1])   permute(yt,[2 3 1])   permute(zt,[2 3 1])];
Tgt = [Rgt, permute(KJC,[2 3 1]); zeros(1,3,n), ones(1,1,n)];