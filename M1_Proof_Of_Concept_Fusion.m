% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description:   Routine to check whether the fusion of motion capture and medical imaging
%                can reduce the marker misplacement errors
% Process    :   1 - Load BiPlane Xray data
%                2 - Load Motion Capture file of the patient
%                3 - Pelvic Kinematics
%                4 - HJC position: Hara vs EOS in all combination
%                5 - Hip Kinematics
%                6 - RMSD & Range with and without correction 
% Dependencies : https://ch.mathworks.com/matlabcentral/fileexchange/58021-3d-kinematics-and-inverse-dynamics
%                https://github.com/Biomechanical-ToolKit/BTKCore
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

clear all
close all

% Folders
toolboxFolder = 'C:\Users\moissene\Documents\Professionnel\routines\github\FUSION_Biplane_Xrays_Motion_Capture_';
btkFolder     = 'C:\Users\moissene\Documents\Professionnel\routines\btk';
kindynFolder  = 'C:\Users\moissene\Documents\Professionnel\routines\Toolbox_Kinematics_Inverse_Dynamics';
addpath(toolboxFolder);
addpath(kindynFolder);
addpath(btkFolder);
in_folder     = 'C:\Users\moissene\Documents\Professionnel\routines\github\FUSION_Biplane_Xrays_Motion_Capture_\Data';
patientFiles  = dir([in_folder,'\Patient_*']);
npatient      = size(patientFiles,1)/6; % 6 files per patient
% Pelvis markers index
id = {[],'2'};
% Marker Size
m_size = 14; % mm 
% Marker Misplacement Configuration
config(1).name = 'Vertical Offset';
config(2).name = 'Axial Rotation';
config(3).name = 'Anteversion';
config(4).name = 'Obliquity';

%% Main Loop - Kinematic computations for all patients & all configurations
for i = 1:npatient % Loop on patients
% 1 - Load BiPlane Xray data
    csv_file = dir([in_folder,'\',patientFiles(i*6-5).name(1:9),'*.csv']);
    [csv] =  importdata([in_folder,'\',csv_file(end).name],',');
    % Make a structure with BPXR mrk
    for j = 1:size(csv.data,1) % Loop on marker
       if strcmp(csv.textdata{j+1,1}(1:2),'NM') == 0 % Is mrk a marker?
           if csv.data(j,4) == 0
           me.(csv.textdata{j+1,1}) = csv.data(j,1:3);
           else 
               me.(csv.textdata{j+1,1}) = csv.data(j,1:3);
               me.([csv.textdata{j+1,1},'_r']) = csv.data(j,4);
           end
       else % Not a marker           
       end
    end % Loop on markers
    
    % Distance between BPXR markers 
    d.rasi(i) = norm(me.RASI - me.RASI2); 
    d.lasi(i) = norm(me.LASI - me.LASI2); 
    d.rpsi(i) = norm(me.RPSI - me.RPSI2); 
    d.lpsi(i) = norm(me.LPSI - me.LPSI2); 
    
    % Anatomical LCS pelvis, BPXR (Internal LCS)
    [e.Rgpa,e.Tgpa] = f_t_local_pelvis_ANAT(me.RASI_eos,me.LASI_eos,me.PSYM);
        
    % HJC BPXR in LCS anat
        tmp1 = Mprod_array3(Tinv_array3(e.Tgpa),[me.RHJC 1]');
        tmp2 = Mprod_array3(Tinv_array3(e.Tgpa),[me.LHJC 1]');
    HJC(i).Re_pa = tmp1(1:3)';
    HJC(i).Le_pa = tmp2(1:3)';
    
% 2 - Load Motion Capture file of the patient
    % 2.1 - filename
    c3d_file = dir([in_folder,'\',patientFiles(i*6-5).name(1:9),'*Gait.c3d']); 
      
    % 2.2 - Load data
    acq = btkReadAcquisition([in_folder,'\',c3d_file(1).name]);
    m   = btkGetMarkers(acq);          % Structure with markers
    f   = btkGetPointFrequency(acq);   % Acquisition Frequency
    n   = btkGetPointFrameNumber(acq); % Number of frames
    md  = btkGetMetaData(acq);         % Metadata   
    
    % 2.3 - Gait Velocity (Right side)
        tmp1 = (m.RHEE(end,1) - m.RHEE(1,1)) / 1000;  % Distance Heel to Heel, FS to FS
        tmp2 = (n-1)/f;                               % Time betwee FS
    Vgc(i) = tmp1./tmp2;
    clear tmp*
           
    % Loop on pelvis LCS configuration
    cpt = 0; % Index for the configuration number
        for k =1:2 % Loop RASI
            for l = 1:2 % Loop LASI
                for p = 1:2 % Loop RPSI
                    for q = 1:2 % Loop LPSI
                        cpt = cpt + 1;
        
        % 3 - Pelvic Kinematics
        % 3.1 - compute correction matrix
        % 3.1.1. - LCS pelvis in BPXR
        [e.Rgp,e.Tgp] = f_t_local_pelvis_CGM(me.(['RASI',id{k}]),me.(['LASI',id{l}]),...
                                             me.(['RPSI',id{p}]),me.(['LPSI',id{q}]),m_size);
            
        % 3.1.2 - Correction matrices
        Rppa = repmat(Mprod_array3(Minv_array3(e.Rgp),e.Rgpa),[1 1 n]);
        Tppa = repmat(Mprod_array3(Tinv_array3(e.Tgp),e.Tgpa),[1 1 n]);
            
        % 3.2 - Compute Pelvis LCS with markers of motion capture
        [Rgp,Tgp] = f_t_local_pelvis_CGM(m.(['RASI',id{k}]),m.(['LASI',id{l}]),...
                                         m.(['RPSI',id{p}]),m.(['LPSI',id{q}]),m_size);
            
        % 3.3 - Apply correction matrix from BPXR
        Rgpa = Mprod_array3(Rgp,Rppa);
        Tgpa = Mprod_array3(Tgp,Tppa);
            
        % 3.4 - Compute angles without and with correction
        [YXZp, ~] = f_t_R2YXZ(Rgp);
        [YXZpa,~] = f_t_R2YXZ(Rgpa);
        
        % 3.5 - Unwrap
        YXZp  = unwrap(YXZp)/pi*180;
        YXZpa = unwrap(YXZpa)/pi*180;
        
        % 3.6 - Interpolate between 0 and 100% of GC
        t = linspace(0,n,101);
        YXZp  = interp1(1:n,YXZp,t,'spline');
        YXZpa = interp1(1:n,YXZpa,t,'spline');
                
        % 3.7 - Store Kinematics
        p_ang = {'Tilt','Obliquity','Rotation'};
        for a = 1:size(p_ang,2)
            p_ang_r.(p_ang{a})  =  YXZp(:,a);
            p_ang_rc.(p_ang{a}) = YXZpa(:,a);
        end
        
        % Store 
        if cpt == 1 % No Angles were computed yet
            angR(i)  = p_ang_r;
            angRc(i) = p_ang_rc;
            
        else %  Angles were computed, add new angles at the end
            fa = fieldnames(p_ang_r);
            for a = 1:size(fa,1)
                % Right Marker
                    tmp5 = angR(i).(fa{a});
                    tmp6 = p_ang_r.(fa{a});
                angR(i).(fa{a}) = [tmp5 tmp6];
                % Right Marker Corrected
                    tmp7 = angRc(i).(fa{a});
                    tmp8 = p_ang_rc.(fa{a});
                angRc(i).(fa{a}) = [tmp7 tmp8];
                clear tmp*
            end
        end
        
        % 4 - HJC position: Hara vs BPXR in all combination
        % 4.1 - Hara
        % 4.1.1 - position in global
        rll = max(f_t_norm(m.RASI - m.RANK)); % Right leg length (mm)
        lll = max(f_t_norm(m.RASI - m.RANK)); % Left  leg lenght (mm)
        [RHJC_hara,LHJC_hara] = f_t_HJC_Hara(m.(['RASI',id{k}]),m.(['LASI',id{l}]),...
                                             m.(['RPSI',id{p}]),m.(['LPSI',id{q}]),rll,lll,m_size);
        
        % 4.1.2 - Mean position in LCS pelvis BPXR
        % Right HJC
            tmp1 = permute([RHJC_hara, ones(n,1)],[2 3 1]);
            tmp2 = mean(Mprod_array3(Tinv_array3(Tgpa),tmp1),3);
        HJC(i).R_pa(cpt,:) = tmp2(1:3)'; % In mm
        % Left HJC
            tmp3 = permute([LHJC_hara, ones(n,1)],[2 3 1]);
            tmp4 = mean(Mprod_array3(Tinv_array3(Tgpa),tmp3),3);
        HJC(i).L_pa(cpt,:) = tmp4(1:3)'; % In mm
        clear tmp*
        
        % 4.2 - BPXR HJC in global
        % [HJCe_g;1] = Tgpa * [HJCe_pa;1] 
        % 4.2.1 - Right HJC BPXR in global
            tmp1 = repmat([HJC(i).Re_pa, 1]',[1 1 n]);
            tmp2 = Mprod_array3(Tgpa,tmp1);
        RHJC_bpxr = permute(tmp2(1:3,1,:),[3 1 2]);
        % 4.2.2 - Left HJC BPXR in global
            tmp1 = repmat([HJC(i).Le_pa, 1]',[1 1 n]);
            tmp2 = Mprod_array3(Tgpa,tmp1);
        LHJC_bpxr = permute(tmp2(1:3,1,:),[3 1 2]);
        
        % 5 - Hip Kinematics
        % 5.1 - Knee Joint Centers & Thigh LCS
        Rkw = str2double(md.children.PROCESSING.children.KneeWidth.info.values{1}); % Right Knee Width (mm)
        % 5.1.1 - With HJC Hara
        [Rh_grt,RKJC_hara] = f_t_local_thigh_CGM(Rkw,m.RKNE,RHJC_hara,m.RTHI);
        % 5.1.2 - With HJC BPXR
        [Ra_grt,RKJC_bpxr]  = f_t_local_thigh_CGM(Rkw,m.RKNE,RHJC_bpxr,m.RTHI);
        
        % 5.3 - Hip Kinematics
        % 5.3.1 - Rotation matrices
        % field Rhip: hjc_pelvis_thigh 
        %       hjc: h = Hara / a = Anatomical, defined with BiPlane XRay
        %       pelvis: p = LCS from skin markers / pa = Anatomical, corrected with BiPlane XRay
        %       thigh: rt = right thigh
        % To match CGM sign convention, need to rotate LCS by 180 deg around z
        % Right Hip: (Rgpelvis * Rz180)' * Rgrt
        Rz = f_t_Rot(pi*ones(n,1),3);
        % Right Hip
        Rhip(i).h_p_rt  = Mprod_array3(Minv_array3(Mprod_array3(Rgp,Rz)), Rh_grt); % Right Hip HJC Hara Pelvis Mrk 
        Rhip(i).h_pa_rt = Mprod_array3(Minv_array3(Mprod_array3(Rgpa,Rz)),Rh_grt); % Right Hip HJC Hara Pelvis Anat
        Rhip(i).a_pa_rt = Mprod_array3(Minv_array3(Mprod_array3(Rgpa,Rz)),Ra_grt); % Right Hip HJC EOS  Pelvis Anat
        
        % 5.3.2 - Store Hip Joint Angles
        fr = fieldnames(Rhip(i));
        p_ang = {'FE','AA','IER'};
        for r = 1:size(fr,1) % Loop on kinematic config (Hip Hara/Anat, Pelvis Mrk/Anat)
            % a. Joint Angles
            tmp1 = f_t_R2YXZ(Rhip(i).(fr{r}));                      % Decomposition
            tmp2 = unwrap(tmp1) / pi *180;                          % Unwrap
            tmp3 = interp1(1:n,tmp2,t,'spline');                    % Interp between 0 & 100% of GC
        
            % b. Store
            if cpt == 1 % First Configuration, no angles were computed yet
                for a = 1:size(p_ang,2)
                    hAngR(i).(fr{r}).(p_ang{a}) = tmp3(:,a);
                end
            else %  Angles were computed, add new angles at the end
                for a = 1:size(p_ang,2)
                    % Right Hip Kinematics
                        tmp5 = hAngR(i).(fr{r}).(p_ang{a}); % Angle of the previous configuration
                        tmp6 = tmp3(:,a);                   % Angle of the new configuration
                     % Put new configuration at the end of previous ones (all configurations) at the end
                    hAngR(i).(fr{r}).(p_ang{a}) = [tmp5 tmp6]; 
                end
                clear tmp*
            end
        end % Loop on kinematic config (L/R, Hip Hara/Anat, Pelvis Mrk/Anat)       
      
                    end % Loop LPSI
                end % Loop RPSI
            end % Loop LASI
        end % Loop RASI
    
% 6 - RMSD & Range with and without correction per patient
    % 7.1 - SD
    nc = k*l*p*q;                                                          % Number of marker configuration
    rotp = fieldnames(angR);                                               % Pelvis Angles
    ch   = fieldnames(hAngR);                                              % Configurations Hip Kin
    roth = fieldnames(hAngR(i).(ch{1}));                                   % Hip Joint Angles 
    n_rg = 1;                                                              % Number of Right Gait Cycles
        
    % 6.1.1 - Pelvis Kinematics Right Gait Cycle
    for r = 1:size(rotp,1)
        % a - SD
        tmp_R  = mean(std( angR(i).(rotp{r}),1,2));
        tmp_Rc = mean(std(angRc(i).(rotp{r}),1,2));
        % b - RMSD Marker Based
            tmp1 = mean(angR(i).(rotp{r}),2);  % Mean
            tmp2 = angR(i).(rotp{r}) - tmp1;   % Deviation
        tmp_rmsdR = sqrt(mean(mean(tmp2.^2))); % Root Mean Square
        % c - RMSD With Correction Matrix
            tmp3 = mean(angRc(i).(rotp{r}),2); % Mean
            tmp4 = angRc(i).(rotp{r}) - tmp3;  % Deviation
        tmp_rmsdRc = sqrt(mean(mean(tmp4.^2)));% Root Mean Square
        clear tmp1 tmp2 tmp3 tmp4
        % d - Mean SD for the patient
        SD.angR.(rotp{r})(i)  = mean(tmp_R);
        SD.angRc.(rotp{r})(i) = mean(tmp_Rc);
        % e - RMSD for the patient
        RMSD.angR.(rotp{r})(i)  = round(mean(tmp_rmsdR),1);
        RMSD.angRc.(rotp{r})(i) = round(mean(tmp_rmsdRc),1);
    end
    
    % 6.1.2 - Right Hip Kinematics
    for cf =1:size(ch,1)
        for r = 1:size(roth,1)
            % a - SD
            tmp_sdhR  = mean(std( hAngR(i).(ch{cf}).(roth{r}),1,2));
            % b - RMSD
            tmp1 = mean(hAngR(i).(ch{cf}).(roth{r}),2); % Mean
            tmp2 = hAngR(i).(ch{cf}).(roth{r}) - tmp1;  % Deviation
            tmp_rmsdhR = sqrt(mean(mean(tmp2.^2)));     % Root Mean Square
            % c - Mean SD for the patient
            SD.hAngR.(ch{cf}).(roth{r})(i)  = round(tmp_sdhR,1);
            % d - Mean RMSD for the patient
            RMSD.hAngR.(ch{cf}).(roth{r})(i)  = round(tmp_rmsdhR,1);
        end
    end

    % 6.2 - Range
    % 6.2.1 - Pelvis Kinematics Right Gait Cycle
    for r = 1:size(rotp,1)
        % Range
        Range.angR.(rotp{r})(i)  = round(mean((max( angR(i).(rotp{r}),[],2) - min( angR(i).(rotp{r}),[],2))),1);
        Range.angRc.(rotp{r})(i) = round(mean((max(angRc(i).(rotp{r}),[],2) - min(angRc(i).(rotp{r}),[],2))),1);
    end
    
    % 6.2.2 - Right Hip Kinematics
    for cf =1:size(ch,1)
        for r = 1:size(roth,1)
            % tmp variables
            mx = max(hAngR(i).(ch{cf}).(roth{r}),[],2);
            mn = min(hAngR(i).(ch{cf}).(roth{r}),[],2);
            tmp_rhR = mean(mx - mn);
            % Range
            Range.hAngR.(ch{cf}).(roth{r})(i)  = round(mean(tmp_rhR),1);
            clear tmp_rhR mx mn
        end
    end
    
end % Loop on patients
clear tmp*
% END Main loop 

save('Data_Proof_Concept_Fusion','Rhip','angR','angRc','hAngR','config','RMSD','SD','Range','HJC','Vgc','d')

