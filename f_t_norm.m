% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description: Compute the norm of the time vector V, with X,Y,Z in column and time in
%              line, or quaternion q, with q1,q2,q3,q4 in column and time in line
% Inputs     : V - Time vector
% Outputs    : N_V - Norm of the time vector
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [N_V] = f_t_norm(V)

type = size(V,2);
switch type
    case 3 % Vector
        N_V=sqrt(V(:,1).*V(:,1)+V(:,2).*V(:,2)+V(:,3).*V(:,3));
    case 4 % Quaternion
        N_V=sqrt(V(:,1).*V(:,1)+V(:,2).*V(:,2)+V(:,3).*V(:,3)+V(:,4).*V(:,4));
end
