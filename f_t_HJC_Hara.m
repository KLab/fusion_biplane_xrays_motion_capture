% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description:   Hara equation of the HJC based on:
%                Hara et al., Predicting the location of the hip joint 
%                centres, impact of age group and sex, 2016, Scientfic 
%                Report
%                (Regression is for right side, left side was assumed 
%                symmetrical in medial-lateral direction)
% Inputs:        RASI - Position of Right Anterior Superior Iliac Spine (n x 3)
%                LASI - Position of Left  Anterior Superior Iliac Spine (n x 3)
%                RPSI - Position of Right Posterior Superior Iliac Spine (n x 3)
%                LPSI - Position of Left  Posterior Superior Iliac Spine (n x 3)
%                RLL - Right leg length (1 x 1) (mm)
%                LLL - Left leg length (1 x 1) (mm)
%                mrk_size - Marker size (m)
% Outputs:       RHJC - Position of Right Hip Joint Centre (n x 3)
%                LHJC - Position of Right Hip Joint Centre (n x 3)
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [RHJC,LHJC] = f_t_HJC_Hara(RASI,LASI,RPSI,LPSI,RLL,LLL,mrk_size)

% mrk size is in m
% LL is leg length defined in mm

% LCS pelvis Conventional Gait Model
[~,Tgp] = f_t_local_pelvis_CGM(RASI,LASI,RPSI,LPSI,mrk_size);

% RHJC in lcs pelvis
% LL is leg length defined in mm from ASI to the medial maleolus through
% the medial epicondyle
rhjcx =   11 - 0.064* RLL;
rhjcy = -( 8 + 0.086* RLL); % y Pelvis Points left
rhjcz =   -9 - 0.078* RLL;

% RHJC in global
n = size(RASI,1);
 tmp1 = repmat([rhjcx; rhjcy; rhjcz; 1],[1 1 n]);
 tmp2 = Mprod_array3(Tgp,tmp1);
RHJC = permute(tmp2(1:3,:,:),[3 1 2]);

% LHJC in lcs pelvis
% LL is leg length defined in mm from ASI to the medial maleolus through
% the medial epicondyle
lhjcx = 11 - 0.064* LLL;
lhjcy =  8 + 0.086* LLL;
lhjcz = -9 - 0.078* LLL;

% LHJC in global
 tmp3 = repmat([lhjcx; lhjcy; lhjcz; 1],[1 1 n]);
 tmp4 = Mprod_array3(Tgp,tmp3);
LHJC = permute(tmp4(1:3,:,:),[3 1 2]);