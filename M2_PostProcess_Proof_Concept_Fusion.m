% Author     :   X. Gasparutto
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/fusion_biplane_xrays_motion_capture 
% Reference  :   "Can the fusion of motion capture and 3D medical imaging reduce the extrinsic 		
%                variability due to marker misplacements?" 
%                X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand; Plos One
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description:   Process the data from M1_Proof_Of_Concept_Fusion.m
% Process    :    1 - CSV Table outputs
%                 2 - Statistical Tests
%                 3 - Mean Distance Between Markers
%                 4 - Error Hip Joint Center
%                 5 - Offset Hip flexion right side
%                 6 - Figure 3  - Hip joint centre: Regression versus bi-plane X-ray
%                 7 - Figure 4: Pelvis kinematics, standard method versus fusion method.
%                 8 - Figure 5: Overview of the differences of variability for the pelvic angles
%                 9 - Figure 6: Right hip kinematics with the standard method, semi-fusion 
%                10 - Figure 7: Overview of the differences of variability for the hip angles with the standard
% Dependencies : https://ch.mathworks.com/matlabcentral/fileexchange/58021-3d-kinematics-and-inverse-dynamics
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

clear all
close all
clc

% Process data from M1_Proof_Of_Concept_Fusion

% Folders
toolboxFolder = 'C:\Users\moissene\Documents\Professionnel\routines\github\FUSION_Biplane_Xrays_Motion_Capture_';
kindynFolder  = 'C:\Users\moissene\Documents\Professionnel\routines\Toolbox_Kinematics_Inverse_Dynamics';
addpath(toolboxFolder);
addpath(kindynFolder);

% Load data
load Data_Proof_Concept_Fusion
% Param
np = size(angR,2); % n patients
gc = 0:100;
pt = 'abcd';
s  = [1,-1,-1];
lw = 2;

%% 1 - CSV Table outputs
% Table with all SD
% 1.1 - Pelvis Kinematics
% b - Config name (patient)    
for i =1:size(config,2)
    Cp{1,i*2+1} = config(i).name;
end
% c - DoF Pelvis 
Cp{3,1} = 'Tilt';     
Cp{5,1} = 'Obliquity';
Cp{7,1} = 'Rotation'; 
% d - Column with/without correction
for i =1:3
   Cp{i*2+1,2} = 'Standard';
   Cp{i*2+2,2} = 'Fusion';
end
% e - Column SD / Range
for i =1:size(config,2)
   Cp{2,i*2+1} = 'RMSD';
   Cp{2,i*2+2} = 'Range';
end
% f - Fill with data
rotp = fieldnames(RMSD.angR);
for i = 1:size(config,2) % Loop on patients
    for j =1:3 % Loop on DoFs
    % Data without correction
    Cp{j*2+1,i*2+1} = RMSD.angR.(rotp{j})(i);   % RMSD  without correction
    Cp{j*2+1,i*2+2} = Range.angR.(rotp{j})(i);  % Range without correction
    % Data With correction
    Cp{j*2+2,i*2+1} = RMSD.angRc.(rotp{j})(i);  % RMSD  with correction
    Cp{j*2+2,i*2+2} = Range.angRc.(rotp{j})(i); % Range with correction
    end
end

% 1.2 - Hip Kinematics
% b - Config name (patient)    
for i =1:size(config,2)
    Ch{1,i*2+1} = config(i).name;
end
% c - DoF Pelvis 
Ch{3,1} = 'Flexion/Extension';     
Ch{6,1} = 'Adduction/Abduction';
Ch{9,1} = 'Internal/External Rotation'; 
% d - Column with/without correction
for i =1:3
   Ch{i*3,2}   = 'Standard';
   Ch{i*3+1,2} = 'Semi-Fusion';
   Ch{i*3+2,2} = 'Fusion';
end

% e - Column SD / Range
for i =1:size(config,2)
   Ch{2,i*2+1} = 'RMSD';
   Ch{2,i*2+2} = 'Range';
end
% f - Fill with data
roth = fieldnames(RMSD.hAngR.h_p_rt);
for i = 1:size(config,2) % Loop on patients
    for j =1:3 % Loop on DoFs
    % Data without correction
    Ch{j*3,i*2+1}   = RMSD.hAngR.h_p_rt.(roth{j})(i);     % RMSD  without correction
    Ch{j*3,i*2+2}   = Range.hAngR.h_p_rt.(roth{j})(i);    % Range without correction
    % Data Markers & Hip Hara
    Ch{j*3+1,i*2+1} = RMSD.hAngR.h_pa_rt.(roth{j})(i);   % RMSD  with pelvis correction
    Ch{j*3+1,i*2+2} = Range.hAngR.h_pa_rt.(roth{j})(i);  % Range with pelvis correction
    % Data EOS & Hip EOS
    Ch{j*3+2,i*2+1} = RMSD.hAngR.a_pa_rt.(roth{j})(i);  % RMSD  with pelvis & hip correction
    Ch{j*3+2,i*2+2} = Range.hAngR.a_pa_rt.(roth{j})(i); % Range with pelvis & hip correction
    end
end

% 12 - Export csv
Tp = cell2table(Cp);
Th = cell2table(Ch);

writetable(Tp,'Pelvis_Kin_SD_Range_Per_Cycle.csv','Delimiter',';','WriteVariableNames',0)
writetable(Th,'Hip_Kin_SD_Range_Per_Cycle.csv','Delimiter',';','WriteVariableNames',0)

%% 2 - Statistical Tests
% 2.1 - Pelvis
fp = fieldnames(SD.angRc);
rmsdP = []; rmsdPc =[]; rP = []; rPc = [];
for i = 1:size(fp,1)
     % a. SD
     rmsdP  = [rmsdP; RMSD.angR.(fp{i})'] ;
     rmsdPc = [rmsdPc; RMSD.angRc.(fp{i})'] ;
     % b. Range
     rP  = [rP;  Range.angR.(fp{i})'];
     rPc = [rPc; Range.angRc.(fp{i})'];
end

% c. Wilcoxon sign rank
pRMSD_Pelvis  = signrank(rmsdP,rmsdPc);
pRange_Pelvis = signrank(rP,rPc);

% 2.2 - Hip
fp = fieldnames(SD.hAngR.a_pa_rt);
rmsdH = []; rmsdH_P =[]; rmsdH_PH =[]; rH = []; rH_P = []; rH_PH = [];
for i = 1:size(fp,1)
     % a. SD
     rmsdH    = [rmsdH;    RMSD.hAngR.h_p_rt.(fp{i})'] ;  % Standard
     rmsdH_P  = [rmsdH_P;  RMSD.hAngR.h_pa_rt.(fp{i})'] ; % Semi-Fusion
     rmsdH_PH = [rmsdH_PH; RMSD.hAngR.a_pa_rt.(fp{i})'] ; % Fusion
     % b. Range
     rH    = [rH;    Range.hAngR.h_p_rt.(fp{i})'];  % Standard
     rH_P  = [rH_P;  Range.hAngR.h_pa_rt.(fp{i})']; % Semi-Fusion
     rH_PH = [rH_PH; Range.hAngR.a_pa_rt.(fp{i})']; % Fusion
end

% c. Kruskal Wallis tests
kruskalwallis([rH,rH_P,rH_PH]);
kruskalwallis([rmsdH,rmsdH_P,rmsdH_PH]); 

% d. Wilcoxon sign rank
% d.1 - Range
pRange_Hip(1) = signrank(rH,rH_P);    % Standard    vs. Semi-Fusion
pRange_Hip(2) = signrank(rH,rH_PH);   % Standard    vs. Fusion
pRange_Hip(3) = signrank(rH_P,rH_PH); % Semi-Fusion vs. Fusion
% d.2 - RMSD
pRMSD_Hip(1) = signrank(rmsdH,rmsdH_P);    % Standard    vs. Semi-Fusion
pRMSD_Hip(2) = signrank(rmsdH,rmsdH_PH);   % Standard    vs. Fusion
pRMSD_Hip(3) = signrank(rmsdH_P,rmsdH_PH); % Semi-Fusion vs. Fusion
    
%% 3 - Mean Distance Between Markers
fd = fieldnames(d);
for i = 1:size(fd,1)
    tmp1(i) = mean(d.(fd{i})); 
end
md_mrk = mean(tmp1);

%% 4 - Error Hip Joint Center
folder_dicom = {'Data_Select\Patient_a\BPXR\'; ...
                'Data_Select\Patient_b\BPXR\';...
                'Data_Select\Patient_c\BPXR\';...
                'Data_Select\Patient_d\BPXR\'};

file_points =  {'Patient_a_mrkBPXR.csv';...
                'Patient_b_mrkBPXR.csv';...
                'Patient_c_mrkBPXR.csv';...
                'Patient_d_mrkBPXR.csv'};

for j = 1:size(file_points,1)
    % 2 - Load EOS points
    csv = importdata([folder_dicom{j,1},file_points{j,1}]);
    for i = 1:size(csv.data,1)
        m_eos.(csv.textdata{i+1,1}) = csv.data(i,1:3);
        if csv.data(i,4) ~= 0
           r_eos.(csv.textdata{i+1,1}) = csv.data(i,4);
        end
    end
    % 3 - HJC Hara in EOS coordinate system:
    % 3.1 - position in anat pelvis LCS
    RHJC_pa = HJC(1).R_pa;
    LHJC_pa = HJC(1).L_pa;
    % 3.2 - anat pelvis LCS
    [Rgpa,Tgpa] = f_t_local_pelvis_ANAT(m_eos.RHJC(1:3),m_eos.LHJC(1:3),m_eos.PSYM);
    for i = 1:size(RHJC_pa,1)
            tmp1 = [RHJC_pa(i,:) 1]';
            tmp2 = Mprod_array3(Tgpa,tmp1);
        RHJC_e(i,:) = tmp2(1:3)';
            %
            tmp1 = [LHJC_pa(i,:) 1]';
            tmp2 = Mprod_array3(Tgpa,tmp1);
        LHJC_e(i,:) = tmp2(1:3)';
    end
    % Store error between Hara and EOS
    % RMSE
    RMSE.RHJC(j,:) = round(sqrt(mean((RHJC_e - m_eos.RHJC).^2))*10)/10;
    RMSE.LHJC(j,:) = round(sqrt(mean((LHJC_e - m_eos.LHJC).^2))*10)/10;
    % Range of squared error (?)
        tmp1 = max((RHJC_e - m_eos.RHJC).^2);
        tmp2 = min((RHJC_e - m_eos.RHJC).^2);
    RMSE.Rrange(j,:) = round(sqrt(tmp1 - tmp2)*10)/10;
        tmp3 = max((LHJC_e - m_eos.LHJC).^2);
        tmp4 = min((LHJC_e - m_eos.LHJC).^2);
    RMSE.Lrange(j,:) = round(sqrt(tmp3 - tmp4)*10)/10;
end



%% 5 - Offset Hip flexion right side
for i =1:size(hAngR,2)
    Max_Hip_Ext(i) = mean(min(hAngR(i).a_pa_rt.FE));
    SD_Hip_Ext(i) = std(min(hAngR(i).a_pa_rt.FE));
end

%% 6 - Figure 3  - Hip joint centre: Regression versus bi-plane X-ray
% Color
colA = [0 1 0];         
colHE = [0 0 1; 1 0 0];
% 6.1 - Files & Folders
file_dicom   = {'Patient_a_'; ...
                'Patient_b_';...
                'Patient_c_';...
                'Patient_d_'};

file_points =  {'Patient_a_mrkBPXR.csv';...
                'Patient_b_mrkBPXR.csv';...
                'Patient_c_mrkBPXR.csv';...
                'Patient_d_mrkBPXR.csv'};

figure           
for j = 1:size(file_dicom,1)
    % 6.2 - Load DICOM
    IMG_F = dicomread([folder_dicom{j,1},file_dicom{j,1},'Front']);
    xF    = size(IMG_F,2);
    IMG_S = dicomread([folder_dicom{j,1},file_dicom{j,1},'Sagit']);
    info  = dicominfo([folder_dicom{j,1},file_dicom{j,1},'Front']);
    px2mm = info.PixelSpacing(1); 
    h_pelvis = 100:1870;
    % 6.3 - Load EOS points
    csv = importdata([folder_dicom{j,1},file_points{j,1}]);
    for i = 1:size(csv.data,1)
        m_eos.(csv.textdata{i+1,1}) = csv.data(i,1:3);
        if csv.data(i,4) ~= 0
           r_eos.(csv.textdata{i+1,1}) = csv.data(i,4);
        end
    end

    % 6.4 - HJC Hara in Internal Coordinate System:
    % 6.4.1 - position in internal pelvis LCS
    RHJC_pa = HJC(1).R_pa;
    LHJC_pa = HJC(1).L_pa;
    % 6.4.2 - internal pelvis LCS
    [Rgpa,Tgpa] = f_t_local_pelvis_ANAT(m_eos.RASI_eos,m_eos.LASI_eos,m_eos.PSYM);
    for i = 1:size(RHJC_pa,1)
            tmp1 = [RHJC_pa(i,:) 1]';
            tmp2 = Mprod_array3(Tgpa,tmp1);
        RHJC_e(i,:) = tmp2(1:3)';
            %
            tmp1 = [LHJC_pa(i,:) 1]';
            tmp2 = Mprod_array3(Tgpa,tmp1);
        LHJC_e(i,:) = tmp2(1:3)';
    end
    % Store error between Hara and EOS
    RMSE.RHJC(j,:) = round(sqrt(mean((RHJC_e - m_eos.RHJC).^2))*10)/10;
    RMSE.LHJC(j,:) = round(sqrt(mean((LHJC_e - m_eos.LHJC).^2))*10)/10;
    
    % 6.5 - Plot figure
    m_plot = {'RASI','LASI','RASI2','LASI2','RPSI','LPSI','RPSI2','LPSI2'};
    a_plot = {'RASI_eos','LASI_eos','PSYM'}; as = {'v','^','d'};
    r_plot = {'RHJC','LHJC'};
    %
    subplot(2,2,j); 
    imshow([IMG_F(h_pelvis,:),IMG_S(h_pelvis,:)]);title(['Misplacement (',pt(j),') ', config(j).name],'FontSize',18); hold on
    % 6.5.1 - Anat Point
    for i = 1:size(a_plot,2)
        % Front
        plot(m_eos.(a_plot{i})(1)/px2mm, m_eos.(a_plot{i})(2)/px2mm - h_pelvis(1),[as{i},'k'],'MarkerFaceColor',colA,'MarkerSize',8)
        % Sagit
        a0 = plot(m_eos.(a_plot{i})(3)/px2mm + xF, m_eos.(a_plot{i})(2)/px2mm - h_pelvis(1),[as{i},'k'],'MarkerFaceColor',colA);
        a0.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end

    % 6.5.2 - Radius Hips EOS
    for i =1:size(r_plot,2)
        % Front
        xf = m_eos.(r_plot{i})(1)/px2mm;
        yf = m_eos.(r_plot{i})(2)/px2mm - h_pelvis(1);
        rf = r_eos.(r_plot{i})(1)/px2mm;
        trace_cercle(xf,yf,rf,colHE(i,:),'--',2);
        % Sagit
        xs = m_eos.(r_plot{i})(3)/px2mm + xF;
        ys = m_eos.(r_plot{i})(2)/px2mm - h_pelvis(1);
        rs = r_eos.(r_plot{i})(1)/px2mm;
        trace_cercle(xs,ys,rs,colHE(i,:),'--',2);
    end; clear xf yf rf xs ys rs

    % 6.5.3 - Hips Hara
    col2 = [0 1 0];
    for i = 1:size(RHJC_e,1)
        % Front
        a1 = plot(RHJC_e(i,1)/px2mm, RHJC_e(i,2)/px2mm - h_pelvis(1),'ok','MarkerFaceColor',[0 1 1]);
        b1 = plot(LHJC_e(i,1)/px2mm, LHJC_e(i,2)/px2mm - h_pelvis(1),'ok','MarkerFaceColor',[1 1 0]);
        if i~=1
            a1.Annotation.LegendInformation.IconDisplayStyle = 'off';
            b1.Annotation.LegendInformation.IconDisplayStyle = 'off';
        end
        % Sagit
        a2 = plot(RHJC_e(i,3)/px2mm + xF, RHJC_e(i,2)/px2mm - h_pelvis(1),'ok','MarkerFaceColor',[0 1 1]);
        b2 = plot(LHJC_e(i,3)/px2mm + xF, LHJC_e(i,2)/px2mm - h_pelvis(1),'ok','MarkerFaceColor',[1 1 0]);
        a2.Annotation.LegendInformation.IconDisplayStyle = 'off';
        b2.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end

    % 6.5.4 - Center Hips EOS
    for i =1:size(r_plot,2)
        % Front
        x = m_eos.(r_plot{i})(1)/px2mm;
        y = m_eos.(r_plot{i})(2)/px2mm - h_pelvis(1);
        plot(x, y,'ok','MarkerFaceColor',colHE(i,:))
        % Sagit
        x = m_eos.(r_plot{i})(3)/px2mm + xF;
        y = m_eos.(r_plot{i})(2)/px2mm - h_pelvis(1);
        a3 = plot(x, y,'ok','MarkerFaceColor',colHE(i,:));
        a3.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end
    % Legend
    if j == 1
    lg = legend('RASI','LASI','PSYM','RHJC Reg.','LHJC Reg.','RHJC XRay','LHJC XRay');
    lg.FontSize = 16;
    end
end


%% 7 - Figure 4: Pelvis kinematics, standard method versus fusion method.
figure
colH = [0 128 255] /255;
c = [91 155 213;  255 0 0] / 255;
font_axis = 16;
fontfig = 14;
Tprot = {'Post(+)/Ant(-) Tilt','Up(+)/Down(-) Obliquity','Int(+)/Ext(-) Rotation'};
ax = [-25 40;-25 20; -25 20];
mis = {'(a) Vertical Offset','(b) Axial Rotation','(c) Anteversion','(d) Obliquity'};
prot = fieldnames(angR);

for i =1:np
   for r =1:3
       fig = subplot(np,3,(i*3-3)+r); hold on
       % Kinematic for legend
       plot(gc, angR(i).(prot{r})(:,1),'color',c(1,:),'LineWidth',lw);
       plot(gc,angRc(i).(prot{r})(:,1),'color',c(2,:),'LineWidth',lw);
       % Fusion kinematic
       plot(gc, angR(i).(prot{r}),'color',c(1,:),'LineWidth',lw);
       plot(gc,angRc(i).(prot{r}),'color',c(2,:),'LineWidth',lw);
       axis([0 100 ax(r,:)])
       if i == 1
          title({['Pelvic ',Tprot{r}];['\rm ',mis{i},' Misplacement']},'FontSize',font_axis)
       else
          title(['\rm ',mis{i},' Misplacement'],'FontSize',font_axis)
       end
       % Plot Option
       if i == np; xlabel('Gait Cycle (%)','FontSize',font_axis); end
       if r == 1; ylabel('Angle (in deg)','FontSize',font_axis); end
       fig.FontSize = fontfig;
       fig.FontName = 'Times New Roman';
   end
   if i == 1; legend('Standard','Fusion'); end
end

%% 8 - Figure 5: Overview of the differences of variability for the pelvic angles
% standard method and fusion method (** for p < 0.001).

figure
fig1 = subplot(1,2,1); boxplot([rmsdP rmsdPc]); title('RMSD Pelvis Angles');
    ylabel('Angle (deg)')
    fig1.XTickLabel = {'Standard','Fusion'};
    fig1.FontSize = 15;
    fig1.YLim = [0 10];
fig2 = subplot(1,2,2); boxplot([rP rPc]); title('Range Pelvis Angles');
    ylabel('Angle (deg)')
    fig2.XTickLabel = {'Standard','Fusion'};
    fig2.FontSize = 15;
    fig2.YLim = [0 40];

%% 9 - Figure 6: Right hip kinematics with the standard method, semi-fusion 
%  method and fusion method
%
figure
font_axis = 16;
fontfig = 14;
%
r = {'h_p_rt';'h_pa_rt';'a_pa_rt'};
c = [91 155 213;  255 200 0 ; 255 0 0] / 255;
rot = fieldnames(hAngR(1).(r{1})); 
Trot = {'Flex(+)/Ext(-)','Abd(+)/Add(-)','Int(+)/Ext(-) Rot.'};
%
axes = [0 100 -40 70;0 100 -20 25;0 100 -40 40];
%
for i =1:np
   for j =1:3
       fig = subplot(np,3,(i*3-3)+j); hold on; 
       for k = 1:3
           % Kinematic for legend
           l1 = plot(gc,hAngR(i).(r{k}).(rot{j}),'color',c(k,:),'LineWidth',lw);
           axis(axes(j,:))
           if j ==3  % Legend 
                % remove all hara points - 1 from legend
                for p = 1:size(l1,1)-1;l1(p).Annotation.LegendInformation.IconDisplayStyle = 'off'; end
           end
       end
       if i == 1
       title({['Hip ',Trot{j}];['\rm ',mis{i},' Misplacement']},'FontSize',font_axis)
       else
           title(['\rm ',mis{i},' Misplacement'],'FontSize',font_axis)
       end
       % Plot Option
       if i == np ; xlabel('Gait Cycle (%)','FontSize',font_axis); end
       if j == 1; ylabel('Angle (in deg)','FontSize',font_axis); end
       fig.FontSize = fontfig;
       fig.FontName = 'Times New Roman';
   end
   if i == 1; legend('Standard','Semi-Fusion','Fusion'); end
end

%% 10 - Figure 7: Overview of the differences of variability for the hip angles with the standard
% method, the semi-fusion method, and the fusion method
figure
fig3 = subplot(1,2,1); boxplot([rmsdH rmsdH_P rmsdH_PH]); title('RMSD Hip Angles')
    ylabel('Angle (deg)')
    fig3.XTickLabel = {'Standard','Semi-Fusion','Fusion'};
    fig3.FontSize = 16;
    fig3.YLim = [0 10];
fig4 = subplot(1,2,2); boxplot([rH rH_P rH_PH]); title('Range Hip Angles')
    ylabel('Angle (deg)')
    fig4.XTickLabel = {'Standard','Semi-Fusion','Fusion'};
    fig4.FontSize = 16;
    fig4.YLim = [0 40];
