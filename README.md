<table width="100%">
    <tr>
        <td width="25%">
            <img src="https://www.unige.ch/medecine/kinesiology/application/files/5615/7313/2686/logo_UNIGE_300.png" alt="Kinesiology Laboratory - University of Geneva" width="100%"/>
        </td>
        <td width="75%">
            <h1>Welcome on the K-LAB GitLab repository</h1><br>
            <p>Thank you for your interest in our projects. You can find further information about our research activites at the University of Geneva on our website: <a href="https://www.unige.ch/medecine/kinesiology" target="_blank">https://www.unige.ch/medecine/kinesiology</a>.</p>
            <p>The projects available on this repository are all freely available and opensource, under the license <a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">Creative Commons Attribution-NonCommercial 4.0 (International License)</a>.</p>
        </td>
    </tr>
</table>
<h2 align="left">PROJECT DESCRIPTION</h2>
The proposed routine can be used to check whether the fusion of motion capture and medical imaging can reduce the marker misplacement errors.
</h2>

## Important Notices
* `master` branch file paths (if exist) are **not** considered stable.

## Table of Contents
[**Installation**](#installation)

[**Features**](#features)

[**Dependencies**](#dependencies)

[**Developer**](#developer)

[**Examples**](#examples)

[**References**](#references)

[**License**](#license)

## Installation
You just need to download or clone the project to use it. Just ensure that you are using Matlab R2018b or newer (The Mathworks, USA).

## Features
* M1_Proof_Of_Concept_Fusion
    * 1 - Load BiPlane Xray data
    * 2 - Load Motion Capture file of the patient
    * 3 - Pelvic Kinematics
    * 4 - HJC position: Hara vs EOS in all combination
    * 5 - Hip Kinematics
    * 6 - RMSD & Range with and without correction 
* M2_PostProcess_Proof_Concept_Fusion
    * 1  - CSV Table outputs
    * 2  - Statistical Tests
    * 3  - Mean Distance Between Markers
    * 4  - Error Hip Joint Center
    * 5  - Offset Hip flexion right side
    * 6  - Figure 3  - Hip joint centre: Regression versus bi-plane X-ray
    * 7  - Figure 4: Pelvis kinematics, standard method versus fusion method.
    * 8  - Figure 5: Overview of the differences of variability for the pelvic angles
    * 9  - Figure 6: Right hip kinematics with the standard method, semi-fusion 
    * 10 - Figure 7: Overview of the differences of variability for the hip angles with the standard

## Dependencies
* 3D Kinematics and Inverse Dynamics toolbox by Raphaël Dumas, freely available here: https://ch.mathworks.com/matlabcentral/fileexchange/58021-3d-kinematics-and-inverse-dynamics
* Biomechanical-ToolKit by Arnaud Barré, freely available here: https://github.com/Biomechanical-ToolKit/BTKCore

## Developer
The proposed routine has been developed by <a href="https://www.unige.ch/medecine/kinesiology/people/xavierg/" target="_blank">Xavier Gasparutto (PhD)</a>, K-Lab, University of Geneva.

## Examples
A dataset is available as example in the following repository: <a href="https://doi.org/10.26037/yareta:md6wej4guvbufkqge322mqyu5i" target="_blank">doi:10.26037/yareta:md6wej4guvbufkqge322mqyu5i</a>

## References
<b><a href="https://doi.org/10.1371/journal.pone.0226648" target="_blank">Can the fusion of motion capture and 3D medical imaging reduce the extrinsic variability due to marker misplacements?</a></b><br>
X. Gasparutto, J. Wegrzyk, K. Rose-Dulcina, D. Hannouche, S. Armand<br>
<i>PLoS ONE 15(1): e0226648.</i>

## License
<a href="https://creativecommons.org/licenses/by-nc/4.0/legalcode" target="_blank">LICENSE</a> © K-Lab, University of Geneva
